# XLSX name ranges processor

Converts named ranges in [`.xlsx`,.`xlsm`,`.xltx`,`.xltm`] files present in the `data/in/files/` into `csv` format. 
**All sheets** are processed. The result files are stored in `data/out/tables/` with manifests as a sliced tables.


**Table of contents:**  
  
[TOC]

## Functional notes

- The processor supports only [`.xlsx`,.`xlsm`,`.xltx`,`.xltm`] files
- All sheets are processed
- It is possible to continue on failure using the parameter flag
- It is possible to select multiple ranges based on regex pattern that will be treated as a single table.
- If same named range is found in multiple files or multiple ranges match the pattern, the number of columns must be the same otherwise the parsing will fail. 
One name range / pattern is considered as a one type of table.
- The result table table is matching the range name (lowercase) or the table name specified in the pattern. 
- Empty column names are automatically generated like `header_i` where `i` is index of the column from 
left hand side.

## Configuration

### Specify ranges

Processor allows two modes of filtering names which are mutually exclusive. If none of the parameters is specified, 
all ranges are downloaded.

Specify mode using one of the following parameters:

- **range_names** - *[OPT]* Extract only specified ranges. 
- **range_patterns** - *[OPT]* A list of objects with following structure: `{"pattern":"result_*", "res_table_name": "results"}`. 
Extract only ranges matching specified `regex` patterns. Property `res_table_name` specifies the result Storage table name.
- **continue_on_failure** - *[OPT]* Default: `0`. If specified or set to 1, the extraction will continue on parsing failures and affected files 
and ranges will be listed in the job log.
- **ignore_missing_ranges** - *[OPT]* Default: `true`. If set to false, the extraction will fail if a 
  specified range name is missing in the input sheets.
- **add_xls_filename** - *[OPT]* `true`/`false`. Default: `false`. Add column containing source file name, named `extracted_filename`. Useful when processing multiple files containing 
name range with the same name.
- **add_range_name** - *[OPT]* `true`/`false`. Default: `false`. Add column containing source range name or pattern, named `extracted_range_name`.
- **header_from_first_row** - *[OPT]* `true`/`false`. Default: `true`. If set to false, first line will be assumed to be data and
and header names will be automatically generated like `header_[i]` where [i] is index of the column. **NOTE** if left to `true` 
first row will be dropped from data and used as header.

### Sample configuration

```json
{
    "definition": {
        "component": "kds-team.processor-xlsx-name-ranges-2-csv"
    },
    "parameters": {
    	    "range_names": [],
    	    "range_patterns": [{"pattern":"Results*", "res_table_name": "results"}],
            "continue_on_failure": 1,
            "add_xls_filename": true,
            "add_range_name": true,
            "header_from_first_row": true
	}
}
```

#### Minimal configuration

Processes all ranges found in all sheets. Does not include filename or range name in the result and fails whenever there's 
a processing error. Taking header from first column.

```json
{
    "definition": {
        "component": "kds-team.processor-xlsx-name-ranges-2-csv"
    }
}
```
 
# Development
 
This example contains runnable container with simple unittest. For local testing it is useful to include `data` folder in the root
and use docker-compose commands to run the container or execute tests. 

If required, change local data folder (the `CUSTOM_FOLDER` placeholder) path to your custom path:
```yaml
    volumes:
      - ./:/code
      - ./CUSTOM_FOLDER:/data
```

Clone this repository, init the workspace and run the component with following command:

```
git clone https://bitbucket.org:kds_consulting_team/kds-team.processor-csv2xlsx.git my-new-component
cd my-new-component
docker-compose build
docker-compose run --rm dev
```

Run the test suite and lint check using this command:

```
docker-compose run --rm test
```

# Testing

The preset pipeline scripts contain sections allowing pushing testing image into the ECR repository and automatic 
testing in a dedicated project. These sections are by default commented out. 

# Integration

For information about deployment and integration with KBC, please refer to the [deployment section of developers documentation](https://developers.keboola.com/extend/component/deployment/) 