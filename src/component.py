import glob
import logging
import os
import sys
from pathlib import Path

import openpyxl
import pandas as pd
import regex
from keboola.component import ComponentBase, UserException
from keboola.component.dao import TableDefinition

# global constants'


# configuration variables
PAR_HEADER_FROM_FIRST_ROW = 'header_from_first_row'
RES_TABLE_NAME = 'res_table_name'
PAR_INCLUDE_FILENAME = 'add_xls_filename'
PAR_INCLUDE_RANGE_NAME = 'add_range_name'

FILENAME_COL_NAME = 'extracted_filename'
FILENAME_RANGE_NAME = 'extracted_range_name'

KEY_SHEET_NAME_PATTERN = 'sheet_name_pattern'
KEY_SHEET_INDEX = 'sheet_indexes'
KEY_RANGE_NAMES = 'range_names'
KEY_RANGE_PATTERNS = 'range_patterns'
KEY_CONTINUE_ON_ERR = 'continue_on_failure'
KEY_CONTINUE_IGNORE_MISSING = 'ignore_missing_ranges'

# #### Keep for debug
KEY_DEBUG = 'debug'
MANDATORY_PARS = []
MANDATORY_IMAGE_PARS = []


class XlsRangeParseError(Exception):
    """
    Custom ex
    """


class Component(ComponentBase):

    def __init__(self, debug=False):
        super().__init__()

        try:
            self.validate_configuration_parameters(MANDATORY_PARS)
            self.validate_image_parameters(MANDATORY_IMAGE_PARS)
        except ValueError as e:
            raise UserException(e)

        # validation
        if self.configuration.parameters.get(KEY_RANGE_NAMES) and self.configuration.parameters.get(KEY_RANGE_PATTERNS):
            raise ValueError(
                'Both range names and range patterns are specified. Please specify only one of the two or none!')

        # check if range patterns is list
        self.validate_range_patterns(self.configuration.parameters.get(KEY_RANGE_PATTERNS))

    @staticmethod
    def validate_range_patterns(range_patterns):
        if range_patterns:
            if not isinstance(range_patterns, list):
                raise ValueError(f"{KEY_RANGE_PATTERNS} must be a list of range pattern dictionaries")
            else:
                for pattern in range_patterns:
                    if not isinstance(pattern, dict):
                        raise ValueError(f"{KEY_RANGE_PATTERNS} must be a list of range pattern dictionaries")
                    elif not pattern.get("pattern") or not pattern.get("res_table_name"):
                        raise ValueError(f"{KEY_RANGE_PATTERNS} must be a list of range pattern dictionaries "
                                         f"containing a defined 'pattern' and 'res_table_name'")

    def run(self):
        '''
        Main execution code
        '''
        params = self.configuration.parameters  # noqa
        files = [f for f in glob.glob(self.files_in_path + "/**", recursive=True)
                 if not f.endswith('.manifest') and Path(f).is_file()]

        results = []
        for f in files:
            results.extend(self.process_sheets(f, params))

        self.write_manifests(results)

    def process_sheets(self, file, params) -> list[TableDefinition]:
        # LOAD FILE
        name = os.path.splitext(os.path.basename(file))[0]
        logging.info(f'Processing file {os.path.basename(file)}')
        wb = openpyxl.load_workbook(file, data_only=True, read_only=False)

        # STATS
        logging.info(f"File loaded: {os.path.basename(file)}")

        logging.info(f'Available sheets {wb.sheetnames}')
        def_names = wb.defined_names
        name_ranges = [b for b in def_names.values() if b.type == 'RANGE']

        name_range_labels = [a.name for a in name_ranges]

        logging.info(f"Available ranges: {name_range_labels}")

        missing_ranges = []

        if params.get(KEY_RANGE_NAMES):
            logging.info(f'Filtering defined range names {params[KEY_RANGE_NAMES]}')
            for label in params.get(KEY_RANGE_NAMES):
                if label not in name_range_labels:
                    logging.info(f'Name range {label} from configuration parameters not in available ranges')
                    missing_ranges.append(label)
            name_ranges = [{'range': n, 'pattern': {'pattern': n.name, RES_TABLE_NAME: n.name}} for n in name_ranges if
                           n.name in params[KEY_RANGE_NAMES]]
        elif params.get(KEY_RANGE_PATTERNS):
            logging.info(f'Filtering defined range patterns {params[KEY_RANGE_PATTERNS]}')
            name_ranges = self._match_by_patterns(name_ranges, params.get(KEY_RANGE_PATTERNS))
        else:
            name_ranges = [{'range': n, 'pattern': {'pattern': n.name, RES_TABLE_NAME: n.name}} for n in name_ranges]
            logging.info('No ranges specified, processing all available.')

        ignore_missing = params.get(KEY_CONTINUE_IGNORE_MISSING, True)

        if missing_ranges and ignore_missing is False:
            raise ValueError(f"Name ranges {missing_ranges} from configuration parameters not in available ranges,"
                             f" either remove them from the configuration or set continue_on_failure to true")

        if not name_ranges:
            logging.warning(
                f"No ranges matching the filter "
                f"'{params.get(KEY_RANGE_NAMES, params.get(KEY_RANGE_PATTERNS))}' found!")
        # process and output
        data = None
        cols = []
        results = []
        for a in name_ranges:
            try:
                data = self.data_frame_from_xlsx_region(wb, a, os.path.basename(file),
                                                        params.get(PAR_INCLUDE_FILENAME, False),
                                                        params.get(PAR_INCLUDE_RANGE_NAME, False),
                                                        params.get(PAR_HEADER_FROM_FIRST_ROW, True))
                cols = list(data.columns)
            except XlsRangeParseError as ex:
                if params.get(KEY_CONTINUE_ON_ERR):
                    logging.warning(
                        f"Could not extract {a['range'].name} range: {ex}! Continue on failure set, skipping.")
                    continue
                else:
                    logging.exception(ex)
                    exit(1)
            except Exception as ex:
                logging.error(f"Could not extract {a['range'].name} range!")
                logging.exception(ex)
                exit(2)

            if data is None:
                continue

            folder_name = a['pattern'][RES_TABLE_NAME].lower().strip()

            # construct result
            tb_def = self.create_out_table_definition(folder_name, schema=cols, incremental=False, has_header=False,
                                                      is_sliced=True)
            if not os.path.exists(Path(tb_def.full_path).as_posix()):
                os.makedirs(Path(tb_def.full_path).as_posix())
            file_name = name + '_' + a['range'].name + '.csv'
            full_path = os.path.join(tb_def.full_path, file_name)

            data.to_csv(full_path, index=False, header=False)
            results.append(tb_def)
            logging.info(f"File {file_name} saved.")
        return results

    def _match_by_patterns(self, name_ranges, patterns):
        res_ranges = list()
        for n in name_ranges:
            res_ranges.extend(
                [{'range': n, 'pattern': pattern} for pattern in patterns if regex.search(pattern['pattern'], n.name)])
        return res_ranges

    def data_frame_from_xlsx_region(self, wb, named_range, filename, include_filename=False, include_range_name=False,
                                    header_from_first_row=True):
        """
        Get a single rectangular region from the specified named region ('invoices')
        into dataframe object. Since its a portion of XLS using Pandas should be OK.
        """

        logging.info(f"Processing range {named_range['range'].name} defined as '{named_range['range'].attr_text}'...")

        # convert to list (openpyxl 2.3 returns a list but 2.4+ returns a generator)
        destinations = list(named_range['range'].destinations)
        if len(destinations) > 1:
            raise XlsRangeParseError(
                f'Range {named_range["range"].name} in workbook contains more than one region!'
            )
        ws, reg = destinations[0]
        # convert to worksheet object (openpyxl 2.3 returns a worksheet object
        # but 2.4+ returns the name of a worksheet)
        try:
            region = wb[ws][reg]

            df = pd.DataFrame([cell.value for cell in row] for row in region)

            # dropping first row
            # Assign header, wipe out first row, rename header
            if header_from_first_row:
                new_header = list(df.iloc[0])
                df = df[1:]
            else:
                new_header = ['' for i in range(len(list(df.iloc[0])))]

            # filename col
            if include_filename:
                df[FILENAME_COL_NAME] = os.path.basename(filename)
                new_header.append(FILENAME_COL_NAME)
            if include_range_name:
                df[FILENAME_RANGE_NAME] = named_range['range'].name
                new_header.append(FILENAME_RANGE_NAME)

            df.columns = new_header

            # check missing header names
            temp_col = df.columns
            cols = []
            i = 0
            for a in temp_col:
                a = str(a).strip().lower()
                if not a or a in ['', 'none', 'nan']:
                    # print('header_{} added'.format(i))
                    cols.append('header_{}'.format(i))
                    i += 1
                else:
                    cols.append(a)
        except Exception as ex:
            logging.warning(
                f'Failed to parse range {named_range["range"].name} defined as "{named_range["range"].attr_text}". '
                f'Error: "{ex}"')
            raise XlsRangeParseError(ex) from ex
        try:
            df.columns = cols
        except Exception as e:
            logging.warning(
                f"Could not rename columns for range {named_range['range'].name} defined as '"
                f"{named_range['range'].attr_text}! {e}")
            raise XlsRangeParseError(
                f"Could not rename columns for range {named_range['range'].name} defined "
                f"as '{named_range['range'].attr_text}! {e}") from e

        return df


"""
        Main entrypoint
"""
if __name__ == "__main__":
    if len(sys.argv) > 1:
        debug_arg = sys.argv[1]
    else:
        debug_arg = False
    try:
        comp = Component(debug_arg)
        comp.run()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except Exception as e1:
        logging.exception(e1)
        exit(1)
